package main

import (
	"github.com/gorilla/websocket"
	"net/http"
)

var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan Message)
var upgrader = websocket.Upgrader{}

type Message struct {
	Username string `json:"username"`
	Token    string `json:"token"`
	Message  string `json:"message"`
}

func handle_conn(w http.ResponseWriter, r *http.Request) {
    log.Info().Msg("websocket init")

	// Update initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
        log.Error().Err(err)
	}

	defer ws.Close()

	clients[ws] = true

	for {
		var msg Message
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
            log.Error().Err(err).Msg("client websocket error:")
			delete(clients, ws)
			break
		}

		username := decode_b64(msg.Username)

		if verify_token(username, msg.Token) {
			broadcast <- msg
            log.Debug().Msg("queued message by: " + username)
		}
	}
}

func handle_messages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		// Send it out to every client that is currently connected
		for client := range clients {
			err := client.WriteJSON(msg)
			if err != nil {
				log.Error().Err(err).Msg("client websocket error:")
				client.Close()
				delete(clients, client)
			}
		}
	}
}
