package main

import (
	"io/ioutil"
	"net/http"
)

func fs(w http.ResponseWriter, r *http.Request) {
    req := r.URL.Path
    req = req[len("/assets/"):]
	mime := get_mime(req)
	w.Header().Set("Content-Type", mime)

	data, err := ioutil.ReadFile(STATIC_DIR + req)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	w.Write([]byte(data))
}
