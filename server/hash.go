package main

import (
	"crypto/sha512"
	"encoding/base64"
)

func hash(data []byte) string {
	hasher := sha512.New()
	hasher.Write(data)
	hash := hasher.Sum(nil)
	return base64.URLEncoding.EncodeToString(hash)
}
