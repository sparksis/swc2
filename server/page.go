package main

import (
	"html/template"
	"io/ioutil"
	"net/http"
)

type Page struct {
	Template string
	Path     string
	Title    string
	Body     template.HTML
}

func (r *Page) render(w http.ResponseWriter) {
	template_path := TEMPLATE_DIR + r.Template + ".html"
	page_path := PAGE_DIR + r.Title + ".html"

	data, _ := ioutil.ReadFile(page_path)
	r.Body = template.HTML(string(data))

	t := template.Must(template.ParseFiles(template_path))
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	t.Execute(w, *r)
}
