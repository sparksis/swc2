package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

func route() {
	r := mux.NewRouter()

	r.PathPrefix(`/ws`).HandlerFunc(handle_conn)
    r.PathPrefix(`/assets/`).HandlerFunc(fs)
	r.PathPrefix(`/api`).HandlerFunc(api)
    r.PathPrefix(`/`).HandlerFunc(render_template)

	http.Handle("/", r)
}
