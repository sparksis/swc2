package main

import (
	"strings"
)

var mime_map map[string]string = make(map[string]string)

func init() {
	mime_map["default"] = "text/plain"
	mime_map["html"] = "text/html"
	mime_map["png"] = "image/png"
	mime_map["jpg"] = "image/jpeg"
	mime_map["css"] = "text/css"
	mime_map["js"] = "application/javascript"
}

func get_mime(str string) string {
	mimetype := mime_map["default"]

	a := strings.Split(str, ".")
	ext := a[len(a)-1]

	if newmime, ok := mime_map[ext]; ok {
		mimetype = newmime
	}

	return mimetype
}
