package main

import (
    "github.com/rs/zerolog"
    "os"
)

const PORT string = "8080"
const STATIC_DIR string = "web/assets/"
const TEMPLATE_DIR string = "web/templates/"
const PAGE_DIR string = "web/pages/"
const HOMEPAGE string = "login"
const DATA_DIR string = "data"
const FAIL_RESPONSE string = "500"
const TOKEN_LEN int = 64

var log = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout, NoColor: true}).With().Timestamp().Logger()

func main() {
    log.Info().Msg("Initializing...")
	fcheck()
    log.Info().Msg("Starting chatserver...")
	go handle_messages()
    log.Info().Msg("Chatserver started.")
    log.Info().Msg("Starting webserver...")
	serve()
}
