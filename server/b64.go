package main

import (
	"encoding/base64"
)

func decode_b64(str string) string {
	data, _ := base64.StdEncoding.DecodeString(str)
	return string(data)
}

func encode_b64(str string) string {
	data := base64.StdEncoding.EncodeToString([]byte(str))
	return data
}
