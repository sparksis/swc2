package main

import (
	"net/http"
	"strings"
)

func render_template(w http.ResponseWriter, r *http.Request) {
    req := r.URL.Path
    req = req[len("/"):]
	pname := HOMEPAGE
	ppath := HOMEPAGE
    if req != "" {
        a := strings.Split(req, "/")
        pname = a[len(a)-1]
		ppath = req
    }
	page := Page{"default", ppath, pname, ""}
	page.render(w)
}
