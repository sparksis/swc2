package main

import (
	"net/http"
)

func serve() {
    log.Info().Msg("Setting up webrouting...")
	route()
    log.Info().Msg("Routing set up.")

    log.Info().Msg("Webserver started.")
	if err := http.ListenAndServe(":"+PORT, nil); err != nil {
        log.Panic().Err(err)
		panic(err)
	}
}
