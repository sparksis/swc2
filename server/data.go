package main

import (
	"io/ioutil"
	"math/rand"
	"os"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func fcheck() {
	if _, err := os.Stat(DATA_DIR); os.IsNotExist(err) {
		os.Mkdir(DATA_DIR, os.ModePerm)
	}

	if _, err := os.Stat(DATA_DIR + "/users"); os.IsNotExist(err) {
		os.Mkdir(DATA_DIR+"/users", os.ModePerm)
	}
}

func create_user(username string, password string) {
	username = encode_b64(username)
	hashpw := []byte(hash([]byte(password)))

	if _, err := os.Stat(DATA_DIR + "/users/" + username); os.IsNotExist(err) {
		os.Mkdir(DATA_DIR+"/users/"+username, os.ModePerm)
	}

	ioutil.WriteFile(DATA_DIR+"/users/"+username+"/password", hashpw, 0644)
}

func verify_user(username string, password string) bool {
	username = encode_b64(username)
	hashpw := hash([]byte(password))
	stored := ""

	data, _ := ioutil.ReadFile(DATA_DIR + "/users/" + username + "/password")
	stored = string(data)

	if stored == hashpw {
		return true
	} else {
		return false
	}
}

func create_token(username string) string {
	username = encode_b64(username)
	bytes := make([]byte, TOKEN_LEN)
	for i := 0; i < TOKEN_LEN; i++ {
		bytes[i] = byte(rand.Int31n(90))
	}
	token := encode_b64(string(bytes))
	ioutil.WriteFile(DATA_DIR+"/users/"+username+"/token", []byte(token), 0644)
	return token
}

func verify_token(username string, token string) bool {
	username = encode_b64(username)
	data, _ := ioutil.ReadFile(DATA_DIR + "/users/" + username + "/token")
	if string(data) == token {
		return true
	} else {
		return false
	}
}
