package main

import (
	"net/http"
)

func api(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	if concat(query["action"]) == "register" {
		username := decode_b64(concat(query["username"]))
		password := decode_b64(concat(query["password"]))
        log.Info().Msg("register " + username)
		create_user(username, password)
	} else if concat(query["action"]) == "get_token" {
		username := decode_b64(concat(query["username"]))
		password := decode_b64(concat(query["password"]))
        log.Info().Msg("get_token " + username)
		if verify_user(username, password) {
			token := create_token(username)
			w.Write([]byte(token))
		} else {
			w.Write([]byte(FAIL_RESPONSE))
		}
	} else if concat(query["action"]) == "verify_token" {
		username := decode_b64(concat(query["username"]))
		token := concat(query["token"])
        log.Info().Msg("verify_token " + username)
		if verify_token(username, token) {
			w.Write([]byte("true"))
		} else {
			w.Write([]byte(FAIL_RESPONSE))
		}
	}
}
