package main

import (
	"strings"
)

func concat(a []string) string {
	return strings.Join(a, "")
}
