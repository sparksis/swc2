#!/usr/bin/env bash

mkdir -p build
go build -o build/swc2 server/*.go
cp -r web build/web
