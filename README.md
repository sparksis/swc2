# swc2

swc2 is a simple & extremely lightweight webchat.

It is easy to deploy on any unix system and is extremely easy to get running

The focus of the project is to have a selfhosted webchat that anyone with a brain can set up and use.
It's scalable from a few users up to hundreds without breaking a sweat.

swc2 is coded in [Go](https://golang.org/) and takes advantage of static linking and the native concurrency of the language.

# Run

## Docker

To run this project via docker simply run the following command:

### Login 

```
docker login registry.gitlab.com
```

### Run

```
docker run -p 8080:8080 -n swc2 registry.gitlab.com/sparksis/swc2:latest
```

# Build

## Runtime Dependencies

Well none, everything is statically linked.
It is recommended to put swc2 behind a nginx reverse proxy though as swc2 doesn't handle TLS/SSL by itself.

## Build Dependencies

* Any recent version of the Go toolchain.

* The following Go packages:

    - [github.com/gorilla/mux](https://github.com/gorilla/mux)

    - [github.com/gorilla/websocket](https://github.com/gorilla/websocket)

    - [github.com/rs/zerolog](https://github.com/rs/zerolog)

## Deployment Instructions

1. Install all build dependencies.

2. Clone this repository & cd into it.

3. Run the `make.sh` script.

4. Copy the contents of the `build` directory wherever you want.

5. Run the binary. The default port is `8080`.

6. Configure a nginx reverse proxy.
