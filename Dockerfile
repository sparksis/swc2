FROM golang:stretch
LABEL Name=swc2 Version=0.0.1
RUN apt-get update && rm -rf /var/lib/apt/lists/*

RUN go get -u github.com/rs/zerolog/log
RUN go get github.com/gorilla/websocket
RUN go get -u github.com/gorilla/mux

RUN mkdir /workspace

COPY . /workspace/

RUN cd /workspace && \
    ./make.sh && \
    cp -rf build /server && \
    ls -l /server/ && \
    rm -rf /workspace
WORKDIR /server
CMD [ "./swc2" ]