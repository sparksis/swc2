var api_url = "http://" + window.location.hostname + port + "/api";
var ws_url = "ws://" + window.location.hostname + port + "/ws";
var fail_response = "500";
var ws;

function register() {
    let username = document.getElementById("userinput").value;
    let password = document.getElementById("passinput").value;

    document.getElementById("userinput").value = "";
    document.getElementById("passinput").value = "";

    let encoded_user = btoa(username);
    let encoded_pass = btoa(password);

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            window.location.href = "/";
        }
    };
    xhttp.open("GET", api_url + "?action=register&username=" + encoded_user + "&password=" + encoded_pass, true);
    xhttp.send();
}

function login() {
    let username = document.getElementById("userinput").value;
    let password = document.getElementById("passinput").value;

    document.getElementById("userinput").value = "";
    document.getElementById("passinput").value = "";

    let encoded_user = btoa(username);
    let encoded_pass = btoa(password);

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let token = this.responseText;
            let userdata = [encoded_user, token];
            storelogin(JSON.stringify(userdata));
            login_continue();
        }
    };
    xhttp.open("GET", api_url + "?action=get_token&username=" + encoded_user + "&password=" + encoded_pass, true);
    xhttp.send();
}

function verify_token(username, token) {
    let flag = false;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let r = this.responseText;
            if (r == "true") {
                flag = true;
            }
        }
    };
    xhttp.open("GET", api_url + "?action=verify_token&username=" + username + "&token=" + token, false);
    xhttp.send();
    return flag;
}

function storelogin(userdata) {
    let storage = window.localStorage;
    storage.setItem("login", userdata);
}

function getlogin() {
    let storage = window.localStorage;
    return storage.getItem("login");
}

function clearlogin() {
    let storage = window.localStorage;
    storage.removeItem("login");
}

function login_continue() {
    let data = JSON.parse(getlogin());
    if (data !== null) {
        let [username, token] = data;
        let is_working = verify_token(username, token);
        if (is_working) {
            window.location.href = "/chat";
        } else {
            clearlogin();
        }
    }
}

function init_chat() {
    ws = new WebSocket(ws_url);
    ws.addEventListener('message', function(e) {
        let msg = JSON.parse(e.data);
        msg.username = atob(msg.username);
        let msg_r = msg.username + " > " + msg.message;
        document.getElementById("chatbox").innerHTML = document.getElementById("chatbox").innerHTML + msg_r + "<br />";
    });

    document.getElementById('messagebox').addEventListener('keydown', (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            send_chat();
        }
    })
}

function send_chat() {
    let userdata = JSON.parse(getlogin());
    let [username, token] = userdata;

    let new_msg = document.getElementById("messagebox").value;
    document.getElementById("messagebox").value = "";

    if (new_msg != '') {
        ws.send(
            JSON.stringify({
                username: username,
                token: token,
                message: new_msg,
            }
        ));
    }
}

function logout() {
    clearlogin();
    window.location.href = "/"
}
